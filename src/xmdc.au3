#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=..\..\..\..\..\..\..\Program Files\AutoIt3\Icons\MyAutoIt3_Blue.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=Загрузка и проверка XML карт
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

 Функции и/или примеры, откуда позаимствован код (частично или полностью):

 compArrsCore
https://www.autoitscript.com/forum/topic/164728-compare-2-arrays-with-3-arrays-as-a-result/?do=findComment&comment=1201986

 _FileInUse
  ( _WinAPI_FileInUse работает некорректно)

  https://www.autoitscript.com/forum/topic/148893-fileopen-detect-already-opened-file/?do=findComment&comment=1059613
  (нерабочий вариант)

  https://www.autoitscript.com/forum/topic/53994-need-help-with-copy-verification/?do=findComment&comment=410020
  (рабочий вариант)

 initGUI
  на конструкторе от
  https://www.isnetwork.at/isn-autoit-studio/

 GUIselectAllTextInEdit
https://www.autoitscript.com/forum/topic/154066-ctrl-a-select-all-expected-keys-produce-ding-sound/?do=findComment&comment=1111996
  (без винапи)
	https://www.autoitscript.com/forum/topic/154066-ctrl-a-select-all-expected-keys-produce-ding-sound/?do=findComment&comment=1111920
  (с винапи)

 findDupsCore
  https://www.autoitscript.com/forum/topic/164666-get-duplicate-from-array/?do=findComment&comment=1201642

 arrRemoveBlanks
  https://www.autoitscript.com/forum/topic/132027-array-delete-blank-element/?do=findComment&comment=919764

#ce ----------------------------------------------------------------------------


; для проверки раскладки клавы, также подтягивает ф-ции WinApi для ctrl+a
#include <WinAPISys.au3>

#include <Array.au3>
; для записи из массивов в файлы
#include <File.au3>
; для выдирания урлов из <loc> </loc>
#include <String.au3>

; для GUI (ISN)
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
; также подтягивает ф-ции WinApi для ctrl+a
#include <GuiButton.au3>
#include <EditConstants.au3>
#include <ProgressConstants.au3>

; для выделения текста в поле edit, также подтягивает ф-ции WinApi для ctrl+a
#include <GuiEdit.au3>

; для распаковки GZIP карт
#include "zavisimosti\ZLIB.au3"


; ---------------- init -------------->>

; заголовок окна задаём вне Scripting.Dictionary, чтобы удобно было работать в ISN
Global $sWinTitle = "Загрузка и проверка XML карт"

#CS
 состояние чекбокса сравнения; кривоватым образом используется:
 есть и локальная переменная, которая тянется в некоторые ф-ции,
 но внедрена и глобальная переменная, чтобы не тянуть это в большое число ф-ций
#CE
Global $iChkboxComp

; данные режимов для MsgBox
Global $oMsgBoxModes = ObjCreate("Scripting.Dictionary")

; назначаем числовые коды иконок и кнопок 1 раз, чтобы потом не использовать цифры;
; не все используются в этом скрипте
$oMsgBoxModes.Add("$iAtt", 48)
$oMsgBoxModes.Add("$iErr", 16)
$oMsgBoxModes.Add("$iInfo", 64)
$oMsgBoxModes.Add("$iQuest", 32)
$oMsgBoxModes.Add("$iQuestNoYes", 292)
$oMsgBoxModes.Add("$iQuestOkCancel", 65)

procKeyboardLayout()


; пути к файлам
Global $oPaths = ObjCreate("Scripting.Dictionary")
$oPaths.Add("$sXmlRel", "sitemap.txt")
$oPaths.Add("$sEmptyXmlRel", "sitemap_empty.txt")
$oPaths.Add("$sDupsRel", "sitemap_dubli.csv")
$oPaths.Add("$sCompRel", "sitemap_diff.csv")
$oPaths.Add("$sXml", @ScriptDir & "\" & $oPaths.Item("$sXmlRel"))
$oPaths.Add("$sEmptyXml", @ScriptDir & "\" & $oPaths.Item("$sEmptyXmlRel"))
$oPaths.Add("$sDups", @ScriptDir & "\" & $oPaths.Item("$sDupsRel"))
$oPaths.Add("$sComp", @ScriptDir & "\" & $oPaths.Item("$sCompRel"))

; для статусов записи в файлы
Global $oWriteEvents = ObjCreate("Scripting.Dictionary")
$oWriteEvents.Add("$iXml", 0)
$oWriteEvents.Add("$iEmptyXml", 0)
$oWriteEvents.Add("$iDups", 0)
$oWriteEvents.Add("$iComp", 0)


; объект, куда будут сохранены хэндлы интерфейса
Global $oGUI = ObjCreate("Scripting.Dictionary")

; используемый метод управлением интерфейсами
Opt("GUIOnEventMode", 1)

initGUI()


prepForRound()
If @error Then Exit

GUISetState(@SW_SHOW, $oGUI.Item("$hGUI"))

; простой GUI ( https://www.autoitscript.com/autoit3/docs/guiref/GUIRef_OnEventMode.htm )
While 1
	Sleep(50)
WEnd


; проверка и изменение раскладки клавиатуры; если при запуске скрипта
; раскладка не была английской, не будет работать выделение ctrl+a в полях edit

Func procKeyboardLayout()

	; пробуем найти окно проводника (окно рабочего стола и активное окно для целей
	; изменения языка почему-то не работают)
	Local $hWin = WinGetHandle("[Class:CabinetWClass]")

	; здесь и далее в этой ф-ции: если не получилось, то молча завершаем
	; чтобы не выкинуть скрипт в ошибку (не будет поддержки ctrl+a , но это не критично)
	If @error Then Return

	Local $sKeyboardLang = _WinAPI_GetKeyboardLayout($hWin)
	If @error Then Return

	If $sKeyboardLang <> 0x04090409 Then
		_WinAPI_SetKeyboardLayout($hWin, 0x04090409)
		If @error Then Return

		showMess( _
				"Необходимо перезапустить скрипт:" & @CRLF _
				& "для корректной работы была изменена раскладка клавиатуры" & @CRLF _
				& "(распознается только при новом запуске).", _
				$oMsgBoxModes.Item("$iAtt") _
				)
		Exit
	EndIf
EndFunc   ;==>procKeyboardLayout

; <<-------------- init ---------------


; ------------ подготовка к раунду ------------->>

Func prepForRound()

	; существуют ли, заняты ли другими процессами
	checkFiles()
	If @error Then Return SetError(1)

	; обнуляем значения статусов
	$oWriteEvents.Item("$iXml") = 0
	$oWriteEvents.Item("$iEmptyXml") = 0
	$oWriteEvents.Item("$iDups") = 0
	$oWriteEvents.Item("$iComp") = 0

EndFunc   ;==>prepForRound


; для предупреждения о перезаписи файлов с результатами,
; удаление файлов предыдущих проверок
Func checkFiles()

	If FileExists($oPaths.Item("$sXml")) _
			Or FileExists($oPaths.Item("$sEmptyXml")) _
			Or FileExists($oPaths.Item("$sDups")) _
			Or FileExists($oPaths.Item("$sComp")) _
			Then

		Local $iContinue = showMess( _
				"В папке:" & @CRLF & @CRLF & @ScriptDir & @CRLF & @CRLF _
				 & "Будет перезаписан один или несколько из следующих файлов:" _
				 & @CRLF & @CRLF & $oPaths.Item("$sXmlRel") & @CRLF _
				 & $oPaths.Item("$sEmptyXmlRel") & @CRLF & $oPaths.Item("$sDupsRel") _
				 & @CRLF & $oPaths.Item("$sCompRel") & @CRLF & @CRLF _
				 & "Продолжить?", _
				$oMsgBoxModes.Item('$iQuestNoYes') _
				)
		If $iContinue <> 6 Then Return SetError(1)

		checkFilesInUse()
		If @error Then Return SetError(2)

		delFiles()
		If @error Then Return SetError(3)
	EndIf
EndFunc   ;==>checkFiles


; проверяем ксв файлы (могут быть открыты в экселе)
Func checkFilesInUse()

	If _FileInUse($oPaths.Item("$sDups")) Or _FileInUse($oPaths.Item("$sComp")) Then

		showMess( _
				"В папке:" & @CRLF & @CRLF & @ScriptDir & @CRLF & @CRLF _
				 & "Один из следующих файлов занят другим процессом:" _
				 & @CRLF & @CRLF & $oPaths.Item("$sDupsRel") & @CRLF _
				 & $oPaths.Item("$sCompRel"), _
				$oMsgBoxModes.Item('$iAtt') _
				)
		Return SetError(1)
	EndIf
EndFunc   ;==>checkFilesInUse


; проверка на занятость файла другим процессом;
Func _FileInUse($sFilePath)

	#CS
	  без этого _WinAPI_CreateFile отдаст тоже 0 при отсутствии файла,
	  как и в случае занятости файла другим процессом, и будет возвращено,
	  что файл используется (хотя не существует на самом деле)
	#CE
	If Not FileExists($sFilePath) Then Return SetError(1, 0, False)

	Local Const $hFileOpen = _WinAPI_CreateFile($sFilePath, 2, 4)

	If $hFileOpen = 0 Then Return True

	_WinAPI_CloseHandle($hFileOpen)
	Return False
EndFunc   ;==>_FileInUse


Func delFiles()

	delFile($oPaths.Item("$sXml"))
	If @error Then Return SetError(1)

	delFile($oPaths.Item("$sEmptyXml"))
	If @error Then Return SetError(2)

	delFile($oPaths.Item("$sDups"))
	If @error Then Return SetError(3)

	delFile($oPaths.Item("$sComp"))
	If @error Then Return SetError(4)

EndFunc   ;==>delFiles


Func delFile($sFile)

	Local $iDeleted = 1
	If FileExists($sFile) Then $iDeleted = FileDelete($sFile)

	If Not $iDeleted Then

		showMess( _
				"В папке:" & @CRLF & @CRLF & @ScriptDir & @CRLF & @CRLF _
				 & "Не получилось удалить один или несколько файлов, требуется удалить их вручную:" _
				 & @CRLF & @CRLF & $oPaths.Item("$sXmlRel") & @CRLF _
				 & $oPaths.Item("$sEmptyXmlRel") & @CRLF & $oPaths.Item("$sDupsRel") _
				 & @CRLF & $oPaths.Item("$sCompRel"), _
				$oMsgBoxModes.Item('$iErr') _
				)
		Return SetError(1)
	EndIf
EndFunc   ;==>delFile

; <<---------- подготовка к раунду ---------------


; --------- управление интерфейсом ---------->>

Func initGUI()
	Local $hGUI
	Local $hInputUrl
	Local $hChkboxDups
	Local $hChkboxComp
	Local $hEditField
	Local $hBtnStart
	Local $hProgbar
	Local $hBtnClearEdit


	$hGUI = GUICreate($sWinTitle, 375, 351, -1, -1, -1, -1)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit", $hGUI)
	$hInputUrl = GUICtrlCreateInput("https://site.ru/sitemap.xml", 22, 39, 287, 23, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "полный путь к XML карте или к индексному файлу карты")
	GUICtrlCreateLabel("Адрес XML карты:", 22, 14, 119, 15, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	$hChkboxDups = GUICtrlCreateCheckbox("Поиск дублей", 22, 78, 109, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hChkboxComp = GUICtrlCreateCheckbox("Сравнить со списком URL", 157, 78, 188, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent(-1, "toggleEditField")
	$hEditField = GUICtrlCreateEdit("", 22, 143, 287, 126, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSendMsg(-1, $EM_LIMITTEXT, -1, 0)
	$hBtnStart = GUICtrlCreateButton("Старт", 144, 290, 100, 35, -1, -1)
	GUICtrlSetOnEvent(-1, "main")
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlCreateGroup("Список URL", 22, 116, 330, 153, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hBtnClearEdit = GUICtrlCreateButton("X", 324, 188, 28, 26, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIclearEdit")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "очистить поле списка")

	; для ctrl+a в текстовых полях
	Local $hSelectAll = GUICtrlCreateDummy()
	GUICtrlSetOnEvent(-1, "GUIselectAllTextInEdit")
	Local $aAccelKeys[1][2] = [["^a", $hSelectAll]]
	GUISetAccelerators($aAccelKeys, $hGUI)

	#CS
	 бар лучше ставить завершающим, иначе может не работать анимация;
	 альтернативный код
	 https://www.autoitscript.com/forum/topic/119143-busy-bar-updating-gui-text/?do=findComment&comment=828290
	#CE
	$hProgbar = GUICtrlCreateProgress(22, 298, 323, 20, $PBS_MARQUEE, -1)
	GUICtrlSetState(-1, BitOR($GUI_HIDE, $GUI_DISABLE))

	saveGuiHandles( _
	 $hGUI, $hInputUrl, $hChkboxDups, $hChkboxComp, $hEditField, _
     $hBtnStart, $hBtnClearEdit, $hProgbar _
	)
EndFunc   ;==>initGUI


Func saveGuiHandles( _
  $hGUI, $hInputUrl, $hChkboxDups, $hChkboxComp, $hEditField, _
  $hBtnStart, $hBtnClearEdit, $hProgbar _
  )

	; сохраняем хэндлы в объект GUI
	$oGUI.Add("$hGUI", $hGUI)
	$oGUI.Add("$hInputUrl", $hInputUrl)
	$oGUI.Add("$hChkboxDups", $hChkboxDups)
	$oGUI.Add("$hChkboxComp", $hChkboxComp)
	$oGUI.Add("$hEditField", $hEditField)
	$oGUI.Add("$hBtnStart", $hBtnStart)
	$oGUI.Add("$hBtnClearEdit", $hBtnClearEdit)
	$oGUI.Add("$hProgbar", $hProgbar)
EndFunc


; по снятию/проставлению галки о сравнении со списком URL
; отключаем/включаем поле для списка URL

Func toggleEditField()
	Local $hControl = $oGUI.Item("$hEditField")
	Local $iState = GUICtrlGetState($hControl)

	If $iState = 144 Then
		GUICtrlSetState($hControl, $GUI_ENABLE)
	Else
		GUICtrlSetState($hControl, $GUI_DISABLE)
	EndIf
EndFunc   ;==>toggleEditField


; will make select all text in eny focused edit, as long as it is edit inside
; $hWin winhandle because we bound GUISetAccelerators to $hWin
Func GUIselectAllTextInEdit()

	Local $hWin = _WinAPI_GetFocus()
	Local $sClass = _WinAPI_GetClassName($hWin)
	If $sClass = 'Edit' Then _GUICtrlEdit_SetSel($hWin, 0, -1)

EndFunc   ;==>GUIselectAllTextInEdit


; очистка поля edit
Func GUIclearEdit()
	GUICtrlSetData($oGUI.Item("$hEditField"), "")
EndFunc   ;==>GUIclearEdit


; для перевода интерфейса в режим ожидания с прогресс баром

Func _GUISetStandByState()
	GUICtrlSetState($oGUI.Item("$hEditField"), $GUI_DISABLE)
	GUICtrlSetState($oGUI.Item("$hBtnClearEdit"), $GUI_DISABLE)
	GUICtrlSetState($oGUI.Item("$hInputUrl"), $GUI_DISABLE)
	GUICtrlSetState($oGUI.Item("$hChkboxDups"), $GUI_DISABLE)
	GUICtrlSetState($oGUI.Item("$hChkboxComp"), $GUI_DISABLE)
	GUICtrlSetState($oGUI.Item("$hBtnStart"), BitOR($GUI_HIDE, $GUI_DISABLE))
	GUICtrlSetState($oGUI.Item("$hProgbar"), BitOR($GUI_ENABLE, $GUI_SHOW))
	GUICtrlSendMsg(-1, $PBM_SETMARQUEE, 1, 50)
EndFunc   ;==>_GUISetStandByState


#CS
 для перевода интерфейса в обычный режим (отключение прогресс бара);
 включение/отключение поля edit в зависимости от значения чекбокса сравнения,
 полученного ранее
#CE
Func _GUISetReadyState($iCompare)
	GUICtrlSetState($oGUI.Item("$hInputUrl"), $GUI_ENABLE)
	GUICtrlSetState($oGUI.Item("$hChkboxDups"), $GUI_ENABLE)
	GUICtrlSetState($oGUI.Item("$hChkboxComp"), $GUI_ENABLE)
	GUICtrlSetState($oGUI.Item("$hBtnClearEdit"), $GUI_ENABLE)

	; восстановить состояние поля edit, которое было задано юзером
	GUICtrlSetState($oGUI.Item("$hEditField"), BitOR($GUI_SHOW, $iCompare = 1 ? $GUI_ENABLE : $GUI_DISABLE))

	GUICtrlSetState($oGUI.Item("$hBtnStart"), BitOR($GUI_ENABLE, $GUI_SHOW))
	GUICtrlSetState($oGUI.Item("$hProgbar"), BitOR($GUI_HIDE, $GUI_DISABLE))
	GUICtrlSendMsg(-1, $PBM_SETMARQUEE, 0, 50)
EndFunc   ;==>_GUISetReadyState


#CS
  показ сообщения;
  $iMode - числовой код для msgbox
  $iChkboxState - статус чекбокса сравнения списков
  (только при работе в интерфейсе)
#CE
Func showMess($sMess, $iMode, $iChkboxState = Null)

	#CS
	  элемент объекта $hGUI иногда выкидывает ошибку, если он используется
	  до включения видимости интерфейса, поэтому 2 msgbox-a
	#CE
	If $iChkboxState = Null Then Return MsgBox($iMode, $sWinTitle, $sMess)

	_GUISetReadyState($iChkboxState)
	Return MsgBox($iMode, $sWinTitle, $sMess, 0, $oGUI.Item("$hGUI"))
EndFunc   ;==>showMess


Func _Exit()
	Exit
EndFunc   ;==>_Exit

; <<-------- управление интерфейсом ----------


; ---------- управление работой ------------->>

Func main()

	prepForRound()
	If @error Then Return

	_GUISetStandByState()

	Local $sXmlPath, $iFindDups, $iCompare, $aUserUrls
	getDataFromCtrls($sXmlPath, $iFindDups, $iCompare, $aUserUrls)
	If @error Then Return

	; загрузка xml карт(ы)

	Local $aSiteUrls[0]

	procXml($sXmlPath, $aSiteUrls)
	If @error Then Return

	; поиск дубликатов в xml карте
	If $iFindDups Then findDups($aSiteUrls)

	; код 2 - дубли не найдены
	If @error And @error <> 2 Then Return

	; странный баг - без этой или какой-либо команды, при отсутствии дублей
	; скрипт сюда похоже не доходит
	Sleep(10)

	; сравнение урлов из карты с урлами, указанными юзером
	If $iCompare Then compArrs($aSiteUrls, $aUserUrls)

	; код 1 - списки совпадают
	If @error And @error <> 1 Then Return

	; здесь тоже может быть баг, описанный выше, поэтому, на всякий
	Sleep(10)

	; соорудить сообщение, в зависимости от того, какая инфа была сохранена
	showMess( _
			getFinalMess($iFindDups, $iCompare), _
			$oMsgBoxModes.Item('$iInfo'), _
			$iCompare _
			)
EndFunc   ;==>main


Func getFinalMess($iFindDups, $iCompare)

	Local $sPart1 = "Работа завершена, результаты сохранены в папку:" _
			 & @CRLF & @ScriptDir & @CRLF & @CRLF

	; выводим в сообщение инфу о записи урлов из карты без проверки,
	; потому как если бы не была произведена запись, работа бы уже прекратилась

	Local $sPart2 = $oPaths.Item("$sXmlRel") & "	(адреса из карты)" & @CRLF

	Local $sPart3, $sPart4, $sPart5

	If $oWriteEvents.Item("$iEmptyXml") = 1 Then
		$sPart3 = $oPaths.Item("$sEmptyXmlRel") & "	(пустые XML файлы)" & @CRLF
	EndIf

	If $oWriteEvents.Item("$iDups") = 1 Then
		$sPart4 = $oPaths.Item("$sDupsRel") & "	(дубли в карте)" & @CRLF
	ElseIf $iFindDups Then
		$sPart4 = "дубли в карте не найдены" & @CRLF
	EndIf

	If $oWriteEvents.Item("$iComp") = 1 Then
		$sPart5 = $oPaths.Item("$sCompRel") & "	(различия списков)" & @CRLF
	ElseIf $iCompare Then
		$sPart5 = "список URL совпадает с картой" & @CRLF
	EndIf

	Return $sPart1 & $sPart2 & $sPart3 & $sPart4 & $sPart5

EndFunc   ;==>getFinalMess

; <<---------- управление работой -------------


; ------------- получение данных из интерфейса ------------>>

Func getDataFromCtrls(ByRef $sXmlPath, ByRef $iFindDups, ByRef $iCompare, ByRef $aUserUrls)

	#CS
	  первым обязательно получаем состояние этого чекбокса,
	  т.к. в зависимости от него надо будет включить или
	  выключить поле edit после завершения или прерывания текущей работы
	#CE
	$iCompare = getCtrlVal($oGUI.Item("$hChkboxComp"), 1)

	; костыль в виде назначения глобальной переменной,
	; чтобы использовать состояние чекбокса без добавления
	; лишних параметров в ф-ции
	$iChkboxComp = $iCompare

	$sXmlPath = getCtrlVal($oGUI.Item("$hInputUrl"), 2)
	If @error Then Return SetError(1)

	; получить значения списка урлов, если требуется для работы
	If $iCompare Then

		getEditData($oGUI.Item("$hEditField"), $aUserUrls)
		If @error Then Return SetError(2)
	EndIf

	$iFindDups = getCtrlVal($oGUI.Item("$hChkboxDups"), 1)

EndFunc   ;==>getDataFromCtrls


; получение и, при необходимости, конвертирование значений данных контрола;
; меняем значение только если используется специальный запрос
; на приведение значения из контрола к определённому формату ($iType)

Func getCtrlVal($hControl, $iType = 0)
	Local $sControlVal = GUICtrlRead($hControl)

	Switch $iType
		;чекбокс или радиобатон
		Case 1
			$sControlVal = Number($sControlVal)

			; во включенном состоянии значение 1 (не надо переназначать),
			; в выключенном состоянии значени 4
			If $sControlVal = 4 Then $sControlVal = 0

			; поле для URL карты
		Case 2
			Local $sPattern = 'https?:\/\/(www\.)?[a-z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ!\#\$%&\(\)\*\+,-\.\/:;<=>\?@\[\\\]\^_\{\|\}~]{3,1999}'

			; значение не является урлом
			If Not StringRegExp($sControlVal, $sPattern) Then

				showMess( _
						"Значение адреса XML карты некорректно.", _
						$oMsgBoxModes.Item('$iAtt'), _
						$iChkboxComp _
						)
				Return SetError(1)
			EndIf
	EndSwitch

	Return $sControlVal
EndFunc   ;==>getCtrlVal


;составление массива из значений текстового поля edit

Func getEditData($hControl, ByRef $aUserUrls)
	Local $sList = GUICtrlRead($hControl)

	; предусматриваем случаи, когда перенос строки только @LF или только @CR - вместо @CRLF
	; (для StringSplit нельзя указать несколько вариантов разделителей)
	$sList = StringRegExpReplace($sList, "\R", @CRLF)

	; счётчик пока не ставим, появится позже, после уникализации массива
	$aUserUrls = StringSplit($sList, @CRLF, 1 + 2)

	; если поле edit не заполнено
	If Not StringStripWS($aUserUrls[0], 8) Then

		showMess( _
				"Список URL пуст или заполнен некорректно.", _
				$oMsgBoxModes.Item('$iAtt'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf
EndFunc   ;==>getEditData

; <<------------ получение данных из интерфейса ------------


; --------- обработка xml ------------>>

Func procXml($sXmlPath, ByRef $aSiteUrls)

	#CS
	  некоторые сайты блокируют автоитовского юзерагента, поэтому ставим такой,
	  который очень врядли будет блокироваться;
	  будучи указнным один раз здесь, действует во всех дочерних ф-ях
	  (или во всём скрипте)
	#CE
	HttpSetUserAgent('Googlebot/2.1 (+http://www.google.com/bot.html)')

	Local $aXmlUrls
	procOneXml($sXmlPath, $aXmlUrls, 1)
	If @error Then Return SetError(1)

	; является ли карта индексным файлом
	; (extended в данном случае содержит эту инфу)
	Local $bIndex = @extended = 1 ? True : False

	If Not $bIndex Then

		; добавить найденные в xml файле урлы в массив всех урлов
		addUrls($aSiteUrls, $aXmlUrls, $sXmlPath)
		If @error Then Return SetError(2)

		; работа с индексным файлом карты
	Else
		; для записи адресов корректных карт, но без единого
		; <loc> </loc> (если такие попадутся)
		Local $aEmptyXml[0]

		procXmlIndex($aXmlUrls, $aSiteUrls, $aEmptyXml)
		If @error Then Return SetError(3)

		; запись пустых карт в файл, если была найдена хотя бы 1 шт.
		If UBound($aEmptyXml) > 0 Then
			saveToFile($oPaths.Item("$sEmptyXml"), $aEmptyXml, "$iEmptyXml")
			If @error Then Return SetError(4)
		EndIf
	EndIf

	; установить счётчик элементов
	setArrCounter($aSiteUrls)
	If @error Then Return SetError(5)

	; сохранение урлов из карты в файл
	saveToFile($oPaths.Item("$sXml"), $aSiteUrls, "$iXml")
	If @error Then Return SetError(6)

EndFunc   ;==>procXml


; обработка одного xml файла карты;
; параметр iIndexCheck отвечает за проверку файла на то,
; является ли индексным файлом карты сайта

Func procOneXml($sUrl, ByRef $aSource, $iIndexCheck = 0)

	; для записи содержимого карты
	Local $sSource

	getXmlSource($sUrl, $sSource)
	If @error Then Return SetError(1)

	#CS
	 выдираем урлы из карты; если их нет, дальше работать нет смысла;
	 stringregexp в режиме 3 (выдирание <loc> </loc> ) чуть быстрее по скорости,
	 но очистка массива от <loc> </loc> затягивает процесс более чем в 2 раза
	#CE
	$aSource = _StringBetween($sSource, '<loc>', '</loc>')

	If @error Then

		showMess( _
				"XML файл не содержит адресов.", _
				$oMsgBoxModes.Item('$iAtt'), _
				$iChkboxComp _
				)
		Return SetError(2)
	EndIf

	If $iIndexCheck Then
		If xmlIndex($sSource) Then SetExtended(1)
	EndIf
EndFunc   ;==>procOneXml


; от загрузки карты до получения её содержимого
Func getXmlSource($sUrl, ByRef $sSource)

	; загрузить карту (грузится в двоичном формате)
	Local $dSource = InetRead($sUrl, 1)
	If @error Then

		showMess( _
				"Не получилось загрузить XML файл:" & @CRLF & $sUrl, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf

	; если расширение карты не gzip'овское
	If Not gzipExt($sUrl) Then

		procNoGzipXml($dSource, $sSource, $sUrl)

		; первое условие выполнится, если получены текстовые данные
		If @extended Then
			Return
		ElseIf @error Then
			Return SetError(2)
		EndIf
	EndIf

	unpack($dSource, $sSource, $sUrl)
	If @error Then Return SetError(3)

EndFunc   ;==>getXmlSource


Func gzipExt($sUrl)
	Return StringRegExp($sUrl, '\.(gz|jgz)$') = 1 ? True : False
EndFunc   ;==>gzipExt


Func xmlText(ByRef $sSource)
	Return StringInStr($sSource, '<?xml version=') <> 0 ? True : False
EndFunc   ;==>xmlText


Func xmlIndex(ByRef $sSource)
	Return StringInStr($sSource, '<sitemapindex') <> 0 ? True : False
EndFunc   ;==>xmlIndex


; проверка gzip по двоичному отпечатку
Func xmlGzip(ByRef $dSource)

	; проверять на ошибки не надо, т.к. они здесь исключены
	; из-за отсутствия опциональных параметров ф-ции

	Return StringInStr(String($dSource), '0x1F8B') <> 0 ? True : False
EndFunc   ;==>xmlGzip


; обработка xml файла не с гзиповским расширением
Func procNoGzipXml(ByRef $dSource, ByRef $sSource, $sUrl)

	; преобразовываем двоичные данные в строку
	dToString( _
			$dSource, _
			$sSource, _
			"Не получилось обработать загруженный XML файл:" & @CRLF & $sUrl _
			)
	If @error Then Return SetError(1)

	If xmlText($sSource) Then Return SetExtended(1)

	; данные не xml и без расширения gzip'овского,
	; поэтому проверяем, является ли файл gzip'овским
	If Not xmlGzip($dSource) Then

		showMess( _
				"Похоже, данный XML файл некорректен:" _
				 & @CRLF & $sUrl, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(2)
	EndIf
EndFunc   ;==>procNoGzipXml


; конвертация двоичных данных в строку
Func dToString(ByRef $dBinary, ByRef $sSource, $sMess)

	$sSource = BinaryToString($dBinary)
	If @error Then

		showMess($sMess, $oMsgBoxModes.Item('$iErr'), $iChkboxComp)
		Return SetError(1)
	EndIf
EndFunc   ;==>dToString


; распаковка GZIP
Func unpack(ByRef $dSource, ByRef $sSource, $sUrl)

	Local $dUnpacked = _ZLIB_GZUncompress($dSource)
	If @error Then

		showMess( _
				"Не получилось распаковать XML файл как архив GZIP:" _
				 & @CRLF & $sUrl, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf

	dToString( _
			$dUnpacked, _
			$sSource, _
			"Не получилось обработать распакованный XML файл:" & @CRLF & $sUrl _
			)
	If @error Then Return SetError(2)

	; к этому моменту содержимое карты должно быть в корректном формате
	If Not xmlText($sSource) Then

		showMess( _
				"XML файл имеет некорректный вид после распаковки:" & @CRLF & $sUrl, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(3)
	EndIf
EndFunc   ;==>unpack


; для добавления урлов из карт(ы) сайта в массив
Func addUrls(ByRef $aSiteUrls, ByRef $aCurrUrls, $sFile)

	_ArrayAdd($aSiteUrls, $aCurrUrls)

	If @error Then
		showMess( _
				"Не получилось обработать адреса из XML файла" & @CRLF & $sFile, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf
EndFunc   ;==>addUrls


; обработка индексного файла карты сайта
Func procXmlIndex(ByRef $aXmlUrls, ByRef $aSiteUrls, ByRef $aEmptyXml)

	Local $aOneXmlUrls, $sXmlPath

	For $i = 0 To UBound($aXmlUrls) - 1

		$sXmlPath = $aXmlUrls[$i]
		procOneXml($sXmlPath, $aOneXmlUrls)

		; если карта пустая
		If @error = 2 Then

			; здесь нет проверки на ошибки, т.к. не надо прерывать работу
			; из-за того, учтётся ли пустая карта или нет
			_ArrayAdd($aEmptyXml, $sXmlPath)
			ContinueLoop

			; если другая ошибка
		ElseIf @error Then
			Return SetError(1)
		EndIf

		addUrls($aSiteUrls, $aOneXmlUrls, $sXmlPath)
		If @error Then Return SetError(2)
	Next
EndFunc   ;==>procXmlIndex

; <<------------- обработка xml ---------------


; ------------ поиск дублей ------------->>

#CS
	для поиска дублей в xml карте сайта;
	обрезаем лишнее из урлов, чтобы провести корректное сравнение;
	возвращать обратно лишнее не нужно, нет смысла и трудозатратно,
	а урл и так откроется
#CE
Func findDups(ByRef $aSiteUrls)

	; оригинальный массив потребуется далее,
	; поэтому работаем с новым
	Local $aCutUrls = $aSiteUrls

	#CS
	  убираем приписку http(s)://(www.), лидирующий слеш, параметры URL;
	  пропускаем счётчик (нулевой элемент);
	  на массив в ~65к урлов уходит ~1,5 c
	#CE
	For $i = 1 To $aCutUrls[0]

		$aCutUrls[$i] = StringRegExpReplace($aCutUrls[$i], 'https?:\/\/(www\.)?(.*?)(\/)?(\?.*)?$', '\2')
	Next

	#CS
	  массив для сохранения инфы о дублях; создаём по количеству полезных элементов
	  обрабатываемого массива, плюс 1 (заголовки столбцов), плюс 1 (счётчик);
	  наверное можно и меньше строк, но вроде надёжней,
	  чтобы массив мог вместить в себя все элементы исходного массива
	#CE
	Local $aRes[$aCutUrls[0] + 2][2] = [["URL", "Число повторов"]]

	findDupsCore($aCutUrls, $aRes)
	If @error = 2 Then

		; дублей не найдено
		Return SetError(2)

	ElseIf @error Then
		Return SetError(1)
	EndIf

	; запись дублей в файл
	saveToFile($oPaths.Item("$sDups"), $aRes, "$iDups", 1)
	If @error Then Return SetError(3)

EndFunc   ;==>findDups


; с числовыми данными работает некорректно - счётчик
; элементов мешает, но так как тут урлы, то норм

Func findDupsCore(ByRef $aOrig, ByRef $aRes)

	Local $iMaxRow = $aOrig[0], $iResArrRow = 1, $iDupCount, $bPrevIsDup

	; плевать, как отсортируется счётчик элементов, он не помешает,
	; а избавление от него трудозатратнее, чем удалить его из массива

	_ArraySort($aOrig)

	If @error Then
		showMess( _
				"Не получилось получить дубли (код 1)", _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf

	#CS
	  начинаем со 1-го элемента, а не с 0-го, потому что 0-й элемент
	  не с чем сравнивать (нет предыдущего элемента)
	#CE
	For $i = 1 To $iMaxRow

		If $aOrig[$i] = $aOrig[$i - 1] Then

			; если предыдущий элемент не является дублем, проводим
			; инициализацию наполнения строки нового массива с инфой о дубле
			If Not $bPrevIsDup Then
				$aRes[$iResArrRow][0] = $aOrig[$i]
				$iResArrRow += 1
				$iDupCount = 0
			EndIf

			; пишем, какой по счёту дубль
			increaseDupCount($aRes, $iResArrRow, $iDupCount)

			#CS
			  переменная статуса о том, что был найден дубль -
			  если следующее значение массива не будет равно текущему,
			  то надо учесть в дублях и текущее значение, т.к. оно тоже является дублем;
			  также, переменная выступает для оценки необходимости
			  инициализации нового ряда в массиве дублей
			#CE
			$bPrevIsDup = True

			; если значение не равно предыдущему
		Else

			; если был найден дубль на предыдущем шаге, то значит предыдущее значение массива
			; равно предыдущему предыдущему, т.е. тоже является дублем, и его тоже надо учесть
			If $bPrevIsDup Then

				; пишем, какой по счёту дубль
				increaseDupCount($aRes, $iResArrRow, $iDupCount)

				#CS
				  обнуляем переменную статуса, чтобы на следующем шаге сработала
				  инициализация нового ряда в массиве дублей при одинаковых значениях,
				  или чтобы не посчитать лишний дубль, если следующее значение не будет равно текущему
				#CE
				$bPrevIsDup = False
			EndIf
		EndIf

		; если последняя итерация и предыдущее значение дубль текущего, то текущее значение
		; тоже надо учесть, т.к. оно не будет учтено на следующей итерации за её отсутствием
		If $i = $iMaxRow And $bPrevIsDup Then

			; пишем, какой по счёту дубль
			increaseDupCount($aRes, $iResArrRow, $iDupCount)
		EndIf
	Next

	; удалить пустые строки нового массива, если он был наполнен хотя бы одним элементом
	If $iResArrRow > 1 Then
		ReDim $aRes[$iResArrRow][2]
	Else
		; дублей не найдено
		Return SetError(2)
	EndIf
EndFunc   ;==>findDupsCore


#CS
	увеличение счётчика дублей для 2д массива;
	$iCount обязательно с ByRef, т.к. изменения должны быть доступны
	там, откуда вызывается ф-ция;
	пишем в предыдущую строку, т.к. после её первоначального наполнения,
	счётчик строки увеличился на 1
#CE
Func increaseDupCount(ByRef $aArray, $iRow, ByRef $iCount)
	$iCount += 1
	$aArray[$iRow - 1][1] = $iCount

EndFunc   ;==>increaseDupCount

; <<---------- поиск дублей ---------------


; ------------ сравнение списков урлов ------------->>

#CS
	для поиска урлов, встречающихся только в xml карте,
	и урлов, встречающихся только в списке юзера
	(предполагается, что список юзера - копипаст из Лягушки
	раздела titles, сформированный без обхода Лягушкой xml карт);

	сравнение списков проводится без обрезки урлов по таким соображениям:

		* если в отчёт попадут по сути одинаковые урлы (не совсем корректный результат), это будет означать кривую настройку сайта и/или карты (в карте должны быть только урлы, отдающие код 200, а на сайте должны быть настроены все редиректы так, чтобы один урл был в единственном экземпляре);

		* такое сравнение покажет кривые урлы в карте или на сайте;
#CE
Func compArrs(ByRef $aSiteUrls, ByRef $aUserUrls)

	Local $aUrlsOnlyXml = ObjCreate("Scripting.Dictionary")
	Local $aUrlsOnlyUser = ObjCreate("Scripting.Dictionary")
	Local $aUrlsBoth = ObjCreate("Scripting.Dictionary")

	; здесь массивы изменяются, ну и хрен с ними, больше они не понадобятся
	cleanArr($aSiteUrls, 1, 1)
	cleanArr($aUserUrls, 0, 1)

	; без кодов ошибок, т.к. хз, куда их там втулить
	compArrsCore($aSiteUrls, $aUserUrls, $aUrlsOnlyXml, $aUrlsOnlyUser, $aUrlsBoth)

	; определяем границу 2д массива, который создадим
	Local $iElsOnlyXml = UBound($aUrlsOnlyXml)
	Local $iElsOnlyUser = UBound($aUrlsOnlyUser)
	Local $iEls = $iElsOnlyXml > $iElsOnlyUser ? $iElsOnlyXml : $iElsOnlyUser
	; плюс 1, т.к. в 2д массиве будут заголовки
	$iEls += 1

	; массивы совпадают, ничего далее делать не нужно
	If $iElsOnlyXml = 0 And $iElsOnlyUser = 0 Then Return SetError(1)

	Local $aRes[$iEls][2] = [["URL отсутствует в карте", "URL присутствует только в карте"]]

	; без проверки на ошибки, т.к. хз, куда их там втулить
	fillArrUniqueVals($aRes, $aUrlsOnlyUser, $aUrlsOnlyXml, $iElsOnlyUser, $iElsOnlyXml)

	; запись результатов в файл
	saveToFile($oPaths.Item("$sComp"), $aRes, "$iComp", 1)
	If @error Then Return SetError(2)

EndFunc   ;==>compArrs


Func compArrsCore(ByRef $a, ByRef $b, ByRef $sda, ByRef $sdb, ByRef $sdc)

	For $i In $a
		$sda.Item($i)
	Next
	For $i In $b
		$sdb.Item($i)
	Next

	For $i In $a
		If $sdb.Exists($i) Then $sdc.Item($i)
	Next
	; элементы, общие для обоих массивов
	$sdc = $sdc.Keys()

	For $i In $sdc
		If $sda.Exists($i) Then $sda.Remove($i)
		If $sdb.Exists($i) Then $sdb.Remove($i)
	Next
	; элементы, уникальные для первого массива
	$sda = $sda.Keys()
	; элементы, уникальные для второго массива
	$sdb = $sdb.Keys()

EndFunc   ;==>compArrsCore


Func fillArrUniqueVals(ByRef $aRes, ByRef $aSrc1, ByRef $aSrc2, $iUboundSrc1, $iUboundSrc2)
	#CS
	  наполняем 2д массив, пропуская заголовки (из-за этого +1);
	  если один из массивов-источников пуст, ошибки выхода за границы массива
	  не будет - итерация завершится не начавшись
	#CE
	For $i = 0 To $iUboundSrc1 - 1
		$aRes[$i + 1][0] = $aSrc1[$i]
	Next

	For $i = 0 To $iUboundSrc2 - 1
		$aRes[$i + 1][1] = $aSrc2[$i]
	Next
EndFunc   ;==>fillArrUniqueVals


; <<---------- сравнение списков урлов ---------------


; --------- общая работа с массивами ---------->>

; поставить нулевым элементом счётчик элементов массива

Func setArrCounter(ByRef $aArray)
	_ArrayInsert($aArray, 0, UBound($aArray))
	If @error Then

		showMess( _
				"Не получилось установить счётчик массива (внутр. ошибка).", _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf
EndFunc   ;==>setArrCounter


; для сохранения массивов в текстовые файлы
Func saveToFile( _
		$sFile, ByRef $aData, $sEventName, $iUtfMode = 0, $sDelimiter = ";", _
		$iBase = Default, $iUBound = Default _
		)

	; режим открытия файла (2 - перезапись; 128 - $FO_UTF8)
	Local $iMode = $iUtfMode = 0 ? 2 : 2 + 128

	Local $hFile = FileOpen($sFile, $iMode)
	If @error Then
		showMess( _
				"Не получилось открыть файл " & $sFile & " для записи данных.", _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(1)
	EndIf

	_FileWriteFromArray($hFile, $aData, $iBase, $iUBound, $sDelimiter)
	If @error Then
		showMess( _
				"Не получилось произвести запись в файл " & $sFile, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(2)
	EndIf

	FileClose($hFile)
	If @error Then
		showMess( _
				"Не получилось корректно закрыть файл " & $sFile, _
				$oMsgBoxModes.Item('$iErr'), _
				$iChkboxComp _
				)
		Return SetError(3)
	EndIf

	$oWriteEvents.Item($sEventName) = 1
EndFunc   ;==>saveToFile


Func cleanArr(ByRef $aArray, $iDelCounterBefore = 0, $iDelCounterAfter = 0)

	; не возвращаем ошибки, т.к. не критично

	If $iDelCounterBefore Then _ArrayDelete($aArray, 0)

	arrRemoveBlanks($aArray)
	$aArray = _ArrayUnique($aArray)

	If $iDelCounterAfter Then _ArrayDelete($aArray, 0)
EndFunc   ;==>cleanArr


; очистка одномерного массива без счётчика в нулевом элементе от пустых строк

Func arrRemoveBlanks(ByRef $aCurr)

	Local $iMaxRow = UBound($aCurr) - 1, $aNew[$iMaxRow + 1], $iRow = 0

	For $i = 0 To $iMaxRow

		; проверка значения на то, что оно НЕ пустое
		If StringStripWS($aCurr[$i], 8) <> "" Then

			; наполняем новый массив только непустыми значениями
			$aNew[$iRow] = $aCurr[$i]
			$iRow += 1
		EndIf
	Next

	; если все строки в оригинальном массиве НЕ пустые
	; (при добавлении всех элементов из старого массива в новый, $iRow будет на 1 больше)
	If $iRow - 1 = $iMaxRow Then
		Return SetError(1)
	Else
		ReDim $aNew[$iRow]
		$aCurr = $aNew
	EndIf
EndFunc   ;==>arrRemoveBlanks

; <<---------- общая работа с массивами ---------------
